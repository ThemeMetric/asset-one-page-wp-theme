<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "asset";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => false,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'redux-framework-demo' ),
        'page_title'           => __( 'Theme Options', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => 'asset',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => '',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => '',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => '',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => '',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '', 'redux-framework-demo' );
    }

    // Add content after the form.
   // $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Header  Fields
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Header', 'redux-framework-demo' ),
        'id'               => 'header-option',
        'desc'             => __( '', 'redux-framework-demo' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home'
    ) );
   Redux::setSection( $opt_name, array(
        'title'            => __( 'Logo ', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'basic-Text',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			
		 array(
                'id'       => 'header-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Header Logo', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your header logo here.Best dimension is 150x50 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			
        )
    ) );
			
   Redux::setSection( $opt_name, array(
        'title'            => __( 'Navigation ', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'nav',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'about',
                'type'     => 'text',
                'title'    => __( 'About', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => 'About',
            ),
			
			 array(
                'id'       => 'whatwedo',
                'type'     => 'text',
                'title'    => __( 'What We do', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => 'What we Do',
            ),
			
			 array(
                'id'       => 'service',
                'type'     => 'text',
                'title'    => __( 'Services', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => 'Services',
            ),
			
			 array(
                'id'       => 'whos',
                'type'     => 'text',
                'title'    => __( 'Who’s ready', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => 'Who’s ready',
            ),
				 array(
                'id'       => 'singsups',
                'type'     => 'text',
                'title'    => __( ' Sign me up', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => ' Sign me up',
            ),
					 array(
                'id'       => 'faqs',
                'type'     => 'text',
                'title'    => __( ' FAQ', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => 'FAQ',
            ),
				 array(
                'id'       => 'contactss',
                'type'     => 'text',
                'title'    => __( 'Contact', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => 'Contact',
            ),
        )
    ) );
	
	 Redux::setSection( $opt_name, array(
        'title'            => __( 'Header Text', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'header-title',
                'type'     => 'text',
                'title'    => __( 'Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'header-description',
                'type'     => 'editor',
                'title'    => __( 'Header description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header section Description or default text is shown.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
        )
    ) );
	
	 Redux::setSection( $opt_name, array(
        'title'            => __( 'Header Background ', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'header-background',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'header-background',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Header Background images', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your header Background image here.Best dimension is 1920x800 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			
			
        )
    ) );
	
  // -> End Header  Fields
  
  
    // -> START About section
    Redux::setSection( $opt_name, array(
        'title'            => __( 'About', 'redux-framework-demo' ),
        'id'               => 'about-us',
        'customizer_width' => '700px',
        'icon'             => 'el el-edit',
    ) );
	
	Redux::setSection( $opt_name, array(
        'title'            => __( 'About Header', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'about-header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'about-header-title',
                'type'     => 'text',
                'title'    => __( 'About Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'about-header-description',
                'type'     => 'editor',
                'title'    => __( 'About Header description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header section Description or default text is shown.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
        )
    ) );
		
		Redux::setSection( $opt_name, array(
        'title'            => __( 'About Images', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'about-images',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
             array(
                'id'       => 'about-img-one',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '1.Image One', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your header Background image here.Best dimension is 1920x800 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'about-img-two',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '2.Image Two', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			
		  array(
                'id'       => 'about-img-three',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '3.Image Three', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			
			  array(
                'id'       => 'about-img-four',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '4.Image Four', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			  array(
                'id'       => 'about-img-five',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '5.Image Five', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			  array(
                'id'       => 'about-img-six',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '6.Image Six', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			
			  array(
                'id'       => 'about-img-seven',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '7.Image Seven', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => '' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			
        )
    ) );
  // -> End About section
  
  
    // -> Start What we do  section
    Redux::setSection( $opt_name, array(
        'title' => __( 'What We Do', 'redux-framework-demo' ),
        'id'    => 'what',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-brush'
    ) );
	
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Header', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'what-header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'what-header-title',
                'type'     => 'text',
                'title'    => __( ' Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'What we do',
				'placeholder' => 'Add Text Here',
            ),
			
		
			
        )
    ) );
	
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Block One', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'blockone-header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'blockone-header-title',
                'type'     => 'text',
                'title'    => __( 'Block One Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'blockone-header-description',
                'type'     => 'editor',
                'title'    => __( 'Block One  description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header section Description or default text is shown.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
        )
    ) );
	
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Block Two', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'blocktwo-header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'blocktwo-header-title',
                'type'     => 'text',
                'title'    => __( 'Block Two Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'blocktwo-header-description',
                'type'     => 'editor',
                'title'    => __( 'Block Two  description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header section Description or default text is shown.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
        )
    ) );
	
	
Redux::setSection( $opt_name, array(
        'title'            => __( 'Block Three', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'blockothree-header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'blockthree-header-title',
                'type'     => 'text',
                'title'    => __( 'Block Three Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'blockthree-header-description',
                'type'     => 'editor',
                'title'    => __( 'Block Three  description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header section Description or default text is shown.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
        )
    ) );
	
// -> End What we do  section

    // -> START Famous Section
    Redux::setSection( $opt_name, array(
        'title' => __( 'Services', 'redux-framework-demo' ),
        'id'    => 'famous',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-wrench'
    ) );

	Redux::setSection( $opt_name, array(
        'title'            => __( 'Service Header', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-header-title',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'famous-header-title',
                'type'     => 'text',
                'title'    => __( 'Service Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-header-description',
                'type'     => 'editor',
                'title'    => __( 'Service Header description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your header section Description or default text is shown.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
        )
    ) );
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Service item One', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-image',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
		
			 array(
                'id'       => 'famous-image-one',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your Famous images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'famous-image-hover-title',
                'type'     => 'text',
                'title'    => __( 'Service images hovet Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-image-hover-description',
                'type'     => 'editor',
                'title'    => __( 'Service hover description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Service item Two', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-image-two',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
		
			 array(
                'id'       => 'famous-image-two',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your Famous images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'famous-image-hover-title-two',
                'type'     => 'text',
                'title'    => __( 'Service images hovet Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-image-hover-description-two',
                'type'     => 'editor',
                'title'    => __( 'Service hover description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Service item Three', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-image-three',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
		
			 array(
                'id'       => 'famous-image-three',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your Famous images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'famous-image-hover-title-three',
                'type'     => 'text',
                'title'    => __( 'Service images hovet Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-image-hover-description-three',
                'type'     => 'editor',
                'title'    => __( 'Service hover description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Service item Four', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-image-four',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
		
			 array(
                'id'       => 'famous-image-four',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your Famous images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'famous-image-hover-title-four',
                'type'     => 'text',
                'title'    => __( 'Service images hovet Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-image-hover-description-four',
                'type'     => 'editor',
                'title'    => __( 'Service hover description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Service item Five', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-image-five',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
		
			 array(
                'id'       => 'famous-image-five',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your Famous images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'famous-image-hover-title-five',
                'type'     => 'text',
                'title'    => __( 'Service images hovet Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-image-hover-description-five',
                'type'     => 'editor',
                'title'    => __( 'Service hover description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Service item Six', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'famous-image-six',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
		
			 array(
                'id'       => 'famous-image-six',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your Famous images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
			   array(
                'id'       => 'famous-image-hover-title-six',
                'type'     => 'text',
                'title'    => __( 'Service images hovet Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'famous-image-hover-description-six',
                'type'     => 'editor',
                'title'    => __( 'Service hover description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
     // -> End Famous Section
	 
	 
	 
	 
    // -> START Number section
    Redux::setSection( $opt_name, array(
        'title' => __( 'Who’s ready', 'redux-framework-demo' ),
        'id'    => 'number-main',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-picture'
		
    ) );
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Who’s ready header', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'number',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'number-header',
                'type'     => 'text',
                'title'    => __( 'Who’s Ready Header Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'number-description',
                'type'     => 'editor',
                'title'    => __( 'Who’s Ready Header description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Who’s ready Item One', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'number-otem',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'number-item-one-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),
                'default'  => 'About Creative Team',
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'number-item-one-description',
                'type'     => 'editor',
                'title'    => __( 'Description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
			array(
                'id'       => 'number-item-one-number',
                'type'     => 'editor',
                'title'    => __( 'Number', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
	
	
	
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Who’s ready Item Two', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'number-tows',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'number-item-two-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'number-item-two-description',
                'type'     => 'editor',
                'title'    => __( 'Description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
			array(
                'id'       => 'number-item-two-number',
                'type'     => 'editor',
                'title'    => __( 'Number', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );


	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Who’s ready Item Three', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'number-tows-three',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'number-item-three-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'number-item-three-description',
                'type'     => 'editor',
                'title'    => __( 'Description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
			array(
                'id'       => 'number-item-three-number',
                'type'     => 'editor',
                'title'    => __( 'Number', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
        )
    ) );
	
	    // -> End Number section


    // -> START Sign me up section
    Redux::setSection( $opt_name, array(
        'title' => __( 'Sign me up', 'redux-framework-demo' ),
        'id'    => 'switch_buttonset',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-cogs'
    ) );
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Background', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'sign',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			 array(
                'id'       => 'signup-background',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Background Image', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add your backround images here.Best dimension is 800x600 px.', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
		
        )
    ) );
	
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Sign me up Header', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'singup-des',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'signup-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'signup-description',
                'type'     => 'editor',
                'title'    => __( 'Description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
					
        )
    ) );
		
 // -> END Sign me up section
    
    // -> START Select Fields
    Redux::setSection( $opt_name, array(
        'title' => __( 'FAQ', 'redux-framework-demo' ),
        'id'    => 'faq',
        'icon'  => 'el el-list-alt'
    ) );
		
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Faq Header', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'faq-des',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'faq-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'faq-description',
                'type'     => 'editor',
                'title'    => __( 'Description', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
		
        )
    ) );
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Faq Question / Answer', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'faqs',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'qus-1',
                'type'     => 'text',
                'title'    => __( '1.Question One', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'ans-1',
                'type'     => 'editor',
                'title'    => __( '1.Answer One', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
			   array(
                'id'       => 'qus-2',
                'type'     => 'text',
                'title'    => __( '2.Question Two', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
				
            ),
			
		array(
                'id'       => 'ans-2',
                'type'     => 'editor',
                'title'    => __( '2.Answer Two', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
				
            ),
			
			array(
                'id'       => 'qus-3',
                'type'     => 'text',
                'title'    => __( '3.Question Three', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
				
            ),
			
		array(
                'id'       => 'ans-3',
                'type'     => 'editor',
                'title'    => __( '3.Answer Three', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
				
            ),
		
			array(
                'id'       => 'qus-4',
                'type'     => 'text',
                'title'    => __( '4.Question Four', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
				
            ),
			
		array(
                'id'       => 'ans-4',
                'type'     => 'editor',
                'title'    => __( '4.Answer Four', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
				
            ),
		
		
			array(
                'id'       => 'qus-5',
                'type'     => 'text',
                'title'    => __( '5.Question Five', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
				
            ),
			
		array(
                'id'       => 'ans-5',
                'type'     => 'editor',
                'title'    => __( '5.Answer Five', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
				
            ),
		
		
			array(
                'id'       => 'qus-6',
                'type'     => 'text',
                'title'    => __( '6.Question Six', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover title or default text is shown.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
				
            ),
			
		array(
                'id'       => 'ans-6',
                'type'     => 'editor',
                'title'    => __( '6.Answer Six', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your hover Description or default text is shown.If you want to add link use anchor tag.', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
				
            ),
		
		
        )
    ) );
	
	
	 // -> START Contact 
    Redux::setSection( $opt_name, array(
        'title' => __( 'Contact', 'redux-framework-demo' ),
        'id'    => 'contact',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-cogs'
    ) );
			
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Contact Us', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'contact-us',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'contact-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),               
				'placeholder' => 'Add Text Here',
            ),
			
		array(
                'id'       => 'contact-address',
                'type'     => 'editor',
                'title'    => __( 'Address', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
			
			array(
                'id'       => 'say-hellow',
                'type'     => 'text',
                'title'    => __( 'Say Hello', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
		
			array(
                'id'       => 'contact-page',
                'type'     => 'text',
                'title'    => __( 'Contact page link', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
		array(
                'id'       => 'social-sharing',
                'type'     => 'text',
                'title'    => __( 'Social sharing !', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'placeholder' => 'Add Text Here',
            ),
		
        )
    ) );
	
	 // -> START Social Link
    Redux::setSection( $opt_name, array(
        'title' => __( 'Social Media', 'redux-framework-demo' ),
        'id'    => 'social',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-adjust-alt'
    ) );
	
	
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Social Media Link', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'social-media',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'facebook',
                'type'     => 'text',
                'title'    => __( 'Facebook', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your Facebook link here.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Link Here',
            ),
			
		array(
                'id'       => 'twitter',
                'type'     => 'text',
                'title'    => __( 'Twitter', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your Twitter link here.', 'redux-framework-demo' ),
                'placeholder' => 'Add Link Here',
            ),
			array(
                'id'       => 'linkedin',
                'type'     => 'text',
                'title'    => __( 'Linkedin', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your Linkedin link here.', 'redux-framework-demo' ),
                'placeholder' => 'Add Link Here',
            ),
			array(
                'id'       => 'pinterest',
                'type'     => 'text',
                'title'    => __( 'Pinterest', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your pinterest link here.', 'redux-framework-demo' ),
                'placeholder' => 'Add Link Here',
            ),
		
        )
    ) );
	
	  Redux::setSection( $opt_name, array(
        'title' => __( 'Footer', 'redux-framework-demo' ),
        'id'    => 'footer',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon'  => 'el el-magic'
    ) );
		
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer Text', 'redux-framework-demo' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'footers',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'footer-text',
                'type'     => 'editor',
                'title'    => __( 'Text', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( 'Add your footer text here.', 'redux-framework-demo' ),               
				'placeholder' => 'Add Link Here',
            ),
			
        )
    ) );
	
	
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

