<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />        
        <title><?php bloginfo('name'); ?></title>
        <meta content="The most beautiful , kits and website templates built for the bootstrap framework. A place for designers and developers." name="description" />
        <meta content=" bootstrap, theme,bootstrap kit, bootstrap template, beautiful ui, awesome ,  css cards,  bootstrap wizard,  bootstrap plugin" name="keywords" />
     
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet" />
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/rubick_pres.css" rel="stylesheet"/>
        <!--     Fonts and icons     -->
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
        <link href="<?php echo get_template_directory_uri() ?>/assets/css/fonts/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/fonts/Rubik-Fonts.css" rel="stylesheet" />
        <link href="<?php bloginfo('stylesheet_url'); ?>">

        <style>
            .section-header .separator {
                margin: 0em auto 2em;
            }
            .space-50{
                height: 50px;
                width: 100%;
                display: block;
                content: "";
            }
            .section .parallax > img{
                width: 100%;
                min-width: 0;
                min-height: 0;
            }
            .pattern-image:after{
                opacity: .2;
                /*                 background: #111; */
            }
            .section-header h1{
                text-shadow: -2px 2px 30px rgba(0, 0, 0, 0.25), -2px 4px 14px rgba(0, 0, 0, 0.1);
            }
            .section-header h5{
                text-shadow: 0px 0px 11px rgba(0, 0, 0, 0.3);
            }
            .section-header .content{
                top: 50%;
            }
            .section-we-are-1 .title{
                max-width: 960px;
            }
            .card .icon ~ h3{
                margin-bottom: 10px;
            }
            .section-with-hover .project .content{
                text-align: center;
            }
            .section-with-hover .project .over-area{
                background: rgba(0, 0, 0, .83);
            }
            .section-clients-2{
                padding: 6em 0 1em;
            }
            .section-clients-2 .nav-text li {
                margin: 0 15px 10px 15px;
            }
            .section-contact-3 .contact-container .address-container{
                width: 28%;
                background-color: #FFFFFF;
                height: 470px;
                top: 50px;
                padding: 20px;
            }
            .section-contact-3 .address{
                margin-top: 40px;
            }
            .section-we-made-3 .content{
                padding: 0 15px;
                text-align: center;
            }
            .section-we-made-3 .over-area .content h3{
                margin: 5px 0 20px;
            }
            .section-we-made-3 .over-area .content p{
                font-size: .9em;
                color: #898989;
            }
            .section-we-made-3 .over-area .content h5{
                margin-bottom: 0px;
                margin-top: 20px;
            }

            .btn-black{
                letter-spacing: 1px;
                margin-top: 20px;
            }

            .copyright a{
                color: #FFFFFF;
            }

            .section-team-2 .team .member p {
                font-size: 1em;
                padding: 0 30px;
            }

            .card{
                margin-bottom: 30px;
            }
            .section-with-hover .project .over-area.over-area-sm .content p {
                font-size: .85em;
            }
            .section-with-hover .project .over-area.over-area-sm .content h4 {
                font-size: 1.6em;
            }
            .logo{
                display: block;
                margin: 0 auto 10px;
                width: 61px;
                height: 61px;
                border-radius: 50%;
                border: 1px solid #333333;
                overflow: hidden;
            }
            .logo img{
                width: 100%;
                height: 100%;
            }
            .loading .loading-container p {
                font-size: 30px;
                width: 220px;
                margin: 0 auto;
                margin-bottom: 30px;
                height: 35px;
            }
            .loading .logo{
                opacity: 0;
                transition: all 0.9s;
                -webkit-transition: all 0.9s;
                -moz-transition: all 0.9s;
            }
            .loading .logo.visible{
                opacity: 1;
            }
            .sharrre .btn{
                color: #444444;
                border-color: #444444;
                font-weight: 400;
            }
            .address .col-md-6{
                padding-right: 7px;
                padding-left: 7px;
            }
            a.img-class{
                opacity: 1;
            }
            a.img-class:hover{
                opacity: .9;
            }
        </style>

        <?php wp_head(); ?>

    </head>
    <body>

        <nav class="navbar navbar-default navbar-transparent navbar-fixed-top navbar-burger" role="navigation">
            <!-- if you want to keep the navbar hidden you can add this class to the navbar "navbar-burger"-->
            <div class="container">
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>										
					
                    <a href="#sliders" class="navbar-brand">										
					<?php if($asset['header-logo'][url]) :?>					
					<img src="<?php echo $asset['header-logo'][url]?>" alt="" class="img-responsive" />					
					<?php else : ?>					
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="" class="img-responsive" />
					
                     <?php endif ?>							
					
                    </a>
                </div>
                <div class="collapse navbar-collapse" >
                    <ul class="nav navbar-nav navbar-right navbar-uppercase">
                        <li class="social-links">
						     <?php if ($asset['facebook']) :?>
							 <a href="<?php echo $asset['facebook'] ?>" target="_blank">
                               <i class="fa fa-facebook-square"></i>
                            </a>
							<?php else: ?>															
                            <a href="https://www.facebook.com/" target="_blank">
                               <i class="fa fa-facebook-square"></i>
                            </a>							
							<?php endif ?>														
							
							<?php if ($asset['twitter']) :?>
							<a href="<?php echo $asset['twitter'] ?>" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
							<?php else: ?>	
							<a href="https://twitter.com/" target="_blank">
							<i class="fa fa-twitter"></i></a>	
							<?php endif ?>                           
                            
							<?php if ($asset['linkedin']) :?>
							  <a href="<?php echo $asset['linkedin']?>" target="_blank">
                                 <i class="fa fa-linkedin-square"></i>
                            </a>
							<?php else: ?>
							  <a href="https://www.linkedin.com" target="_blank">
							<i class="fa fa-linkedin-square"></i></a>		
							<?php endif ?>
								
							<?php if ($asset['pinterest']) :?>
							<a href="<?php echo $asset['pinterest'] ?>" target="_blank">
							<i class="fa fa-pinterest-square"></i></a>
							<?php else: ?>	
							<a href="https://www.pinterest.com/" target="_blank">
							<i class="fa fa-pinterest-square"></i></a>	
							<?php endif ?>								
														                           
                        </li>
						
                        <li>
                            <a href="#" data-scroll="true" data-id="#whoWeAre"> 
                           <?php echo $asset ['about'] ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-scroll="true" data-id="#workflow"> 
                           <?php echo $asset ['whatwedo'] ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-scroll="true" data-id="#projects">
                           <?php echo $asset ['service'] ?>
                            </a>
                        </li>
						 <li>
                            <a href="#" data-scroll="true" data-id="#numbers"> 
                           <?php echo $asset ['whos'] ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-scroll="true" data-id="#uitools">
                            <?php echo $asset ['singsups'] ?>
                            </a>
                        </li>
                       
                        <li>
                            <a href="#" data-scroll="true" data-id="#faq"> 
                           <?php echo $asset ['faqs'] ?>
                            </a>
                        </li>
                       
                        <li>
                            <a href="#" data-scroll="true" data-id="#contact"> 
                           <?php echo $asset ['contactss'] ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
	<div class="wrapper">
            <div class="section section-header" id="sliders">
                <div class="parallax pattern-image">
				   
				   <?php if($asset['header-background'][url]) : ?>
				   
				   <img src="<?php echo $asset['header-background'][url] ?> "/>
				   
				   <?php else : ?>
				   <img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner.jpg"/>
				   
				   <?php endif ?>
				                  
                <!-- <img src="assets/img/header6.jpg"/> -->
                    <div class="container">
                        <div class="content">
                           
							<?php if ($asset['header-title']) :?>
							 <h1><?php echo $asset['header-title'] ?></h1>
					       <?php else: ?>
						  <h1>Creative Team</h1>
					       <?php endif ?>
																					
                            <div class="separator-container">
                                <div class="separator line-separator">∎</div>
                            </div>
							
							<?php if ($asset['header-description']) :?>
							<h5><?php echo $asset['header-description'] ?></h5>
					       <?php else: ?>
						   <h5>Awesome Bootstrap templates to build better websites</h5>
					       <?php endif ?>							
                            
                        </div>
                    </div>
                </div>
                <a href="#" data-scroll="true" data-id="#whoWeAre" class="scroll-arrow hidden-xs hidden-sm">
                <i class="fa fa-angle-down"></i>
                </a>
            </div>
						
            <div class="section section-we-are-1" id="whoWeAre">
                <div class="text-area">
                    <div class="container">
                        <div class="row">
                            <div class="title" id="animationTest">
							<?php if ($asset['about-header-title']) :?>
							<h2><?php echo $asset['about-header-title'] ?></h2>
					        <?php else: ?>		
							<h2>About Creative Team</h2>
					        <?php endif ?>
                                
                                <div class="separator-container">
                                    <div class="separator line-separator">∎</div>
                                </div>
								<?php if ($asset['about-header-description']) :?>
								<p class="large">
										<?php echo $asset['about-header-description'] ?>
                                </p>
								<?php else: ?>		
								<p class="large">
										Creative Team is a startup based in Romania that creates design tools that make the web development process faster and easier. We love the web and care deeply for how users interact with a digital product. We power businesses and individuals to create better looking web projects around the world.
                                </p>
								<?php endif ?>
																											
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card add-animation animation-2">
									<?php if ($asset['about-img-one']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-one']['url'] ?>"/>
									<?php else: ?>	
									<img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic10.jpg" />	
									<?php endif ?>                                     
                                    </div>
                                    <div class="card add-animation animation-4">
									
									<?php if ($asset['about-img-two']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-two']['url'] ?>"/>
									<?php else: ?>	
									<img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic6.jpg" />
									<?php endif ?> 
									  
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card add-animation animation-1">
									<?php if ($asset['about-img-three']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-three']['url'] ?>"/>
									<?php else: ?>	
                                        <img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic11.jpg"/>
									<?php endif ?> 
									
                                    </div>
                                    <div class="card add-animation animation-3">
									<?php if ($asset['about-img-four']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-four']['url'] ?>"/>
									<?php else: ?>	
                                        <img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic1.jpg"/>
									<?php endif ?> 
									
                                    </div>
                                    <div class="card add-animation animation-2">
									<?php if ($asset['about-img-five']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-five']['url'] ?>"/>
									<?php else: ?>	
                                        <img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic9.jpg"/>
									<?php endif ?>
									
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card add-animation animation-3">
									
									<?php if ($asset['about-img-six']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-six']['url'] ?>"/>
									<?php else: ?>	
                                        <img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic2.jpg"/>
									<?php endif ?>
                                    </div>
                                     <div class="card add-animation animation-1">
									 <?php if ($asset['about-img-seven']['url']) :?>
									<img alt="..." src="<?php echo $asset['about-img-seven']['url'] ?>"/>
									<?php else: ?>	
                                        <img alt="..." src="<?php echo get_template_directory_uri(); ?>/assets/img/pic4.jpg"/>
									<?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-we-do-2" id="workflow">
                <div class="container">
                    <div class="row">
                        <div class="title add-animation">
						
						    <?php if ($asset['what-header-title']) :?>
							  <h2><?php echo $asset['what-header-title'] ?></h2>
								<?php else: ?>	
                            <h2>What we do</h2>								
								<?php endif ?>

                            <div class="separator-container">
                                <div class="separator line-separator">∎</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card add-animation animation-1">
                                <div class="icon">
                                    <i class="pe-7s-tools"></i>
                                </div>
								<?php if ($asset['blockone-header-title']) :?>
								<h3><?php echo $asset['blockone-header-title'] ?></h3>								
								<?php else: ?>	
                                 <h3>Create UI Tools</h3>									 								 
								<?php endif ?>
								
								
							<?php if ($asset['blockone-header-description']) :?>
							<p><?php echo $asset['blockone-header-description'] ?> </p>
							<?php else: ?>	
							<p>We have created the tools that will help you in your next design project. From plugins to complex kits, we've got you covered in all areas regarding front-end development. </p>							
							<?php endif ?>
								
																
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card add-animation animation-2">
                                <div class="icon">
                                    <i class="pe-7s-gift"></i>
                                </div>
								<?php if ($asset['blocktwo-header-title']) :?>
								<h3><?php echo $asset['blocktwo-header-title'] ?></h3>								
								<?php else: ?>	
                               <h3>Share Freebies</h3>
                                
								<?php endif ?>
								
								
							<?php if ($asset['blocktwo-header-description']) :?>
							<p><?php echo $asset['blocktwo-header-description'] ?> </p>
							<?php else: ?>	
							<p>Everything that we used to create our platform can be downloaded for free and guess what, people love free stuff. People have already trusted and used them in thousands of sites.</p>								 							
							<?php endif ?>
															
		                         
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card add-animation animation-3">
                                <div class="icon">
                                    <i class="pe-7s-rocket"></i>
                                </div>
								<?php if ($asset['blockthree-header-title']) :?>
								<h3><?php echo $asset['blockthree-header-title'] ?></h3>							
								<?php else: ?>	
                               <h3>Power Hackatons</h3>
                                								 
								<?php endif ?>
								
							<?php if ($asset['blockthree-header-description']) :?>
							<p><?php echo $asset['blockthree-header-description'] ?> </p>
							<?php else: ?>	
							<p>We've sponsored many hackatons around the world with the tools that they need to make awesome web applications. We like to help where we can, and our tools go hand in hand with startups. </p>							
							<?php endif ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    		
          
            
            
            
            
            
            
            
			<!-- gyhj -->
	    <div class="section section-we-made-3 text-center" id="projects">
                <div class="container">
                    <div class="row">
                        <div class="title add-animation">
							<?php if ($asset['famous-header-title']) :?>
							<h2><?php echo $asset['famous-header-title'] ?></h2>
							<?php else: ?>	
								<h2>Services</h2>							
							<?php endif ?>
                            
                            <div class="separator-container">
                                <div class="separator line-separator">∎</div>
                            </div>
							<?php if ($asset['famous-header-description']) :?>
							<p><?php echo $asset['famous-header-description'] ?><br><br></p>
							<?php else: ?>	
									<p>Check out the most popular from our platform. We would love to hear your feedback.<br><br></p>							
							<?php endif ?>
                        </div>
                        
                        
                         <div id="filters" class="button-group">
          <button class="button is-checked" data-filter="*">I AM :</button>
          <button class="button" data-filter=".client">Client</button>
          <button class="button" data-filter=".agent">Agent</button>
          
        </div>
                        
                        
                    </div>
                </div>
         
                <div class="container-fluid">
                    
                    
                    <div class="isotope">
                    
                    <div class="row">  
                         
                       
                         <div class="element-item transition client smash" data-category="transition">
                        
                       
                          
                            <div class="project add-animation animation-1">
							
							
							<?php if ($asset['famous-image-one']['url']) : ?>
							<img src="<?php echo $asset['famous-image-one']['url'] ?> "/>							
							<?php else: ?>								
                           <img src="<?php echo get_template_directory_uri(); ?>/assets/img/opt_GSDK_thumbnail.jpg"/>							
							<?php endif ?>
							                              
                            </div>	
                        <div class="overlay_content text-center">
						   <?php if ($asset['famous-image-hover-title']) :?>
						    <h2><?php echo $asset['famous-image-hover-title'] ?> </h2>
							<?php else: ?>
								<h2>Mountainbike WM</h2>							
							<?php endif ?>
							 
							 
							  <?php if ($asset['famous-image-hover-description']) :?>
							   <p><?php echo $asset['famous-image-hover-description'] ?> </p>
								<?php else: ?>	
						 <p>Branding Print WEB</p>								
								<?php endif ?>
							</div>							
                      
				
                         </div>
                        
                        <div class="element-item agent smash" data-category="metalloid">
                        
                       
                            <div class="project add-animation animation-2">
							
							
							<?php if ($asset['famous-image-two']['url']) : ?>
							<img src="<?php echo $asset['famous-image-two']['url'] ?> "/>							
							<?php else: ?>								
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/opt_coming_sssoon_thumbnail.jpg"/>							
							<?php endif ?>
                                  
                            </div>
							  <div class="overlay_content text-center">
						   <?php if ($asset['famous-image-hover-title-two']) :?>
						    <h2><?php echo $asset['famous-image-hover-title-two'] ?> </h2>
							<?php else: ?>
								<h2>Mountainbike WM</h2>							
							<?php endif ?>
							 
							 
							  <?php if ($asset['famous-image-hover-description-two']) :?>
							   <p><?php echo $asset['famous-image-hover-description-two'] ?> </p>
								<?php else: ?>	
						 <p>Branding Print WEB</p>								
								<?php endif ?>
							</div>
                        
                             </div>
                        
                        
                        <div class="element-item post-transition client smash" data-category="post-transition">

                       
                            <div class="project add-animation animation-3">
							
							<?php if ($asset['famous-image-three']['url']) : ?>
							<img src="<?php echo $asset['famous-image-three']['url'] ?> "/>							
							<?php else: ?>								
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/opt_wizard_thumbnail.jpg"/>					
							<?php endif ?>
                                                                                              
                                
                            </div>
							 <div class="overlay_content text-center">
						   <?php if ($asset['famous-image-hover-title-three']) :?>
						    <h2><?php echo $asset['famous-image-hover-title-three'] ?> </h2>
							<?php else: ?>
								<h2>Mountainbike WM</h2>							
							<?php endif ?>
							 							 
							  <?php if ($asset['famous-image-hover-description-three']) :?>
							   <p><?php echo $asset['famous-image-hover-description-three'] ?> </p>
								<?php else: ?>	
						 <p>Branding Print WEB</p>								
								<?php endif ?>
							</div>
                        </div>
                            

                        
                    </div>                                        
                        
                    <div class="row">
                         <div class="element-item post-transition agent smash" data-category="post-transition">
                       
                            <div class="project add-animation animation-1">
							
							
                              <?php if ($asset['famous-image-four']['url']) : ?>
							<img src="<?php echo $asset['famous-image-four']['url'] ?> "/>							
							<?php else: ?>								
                           <img src="<?php echo get_template_directory_uri(); ?>/assets/img/opt_lbd_thumbnail.jpg"/>
                        					
							<?php endif ?>
                                                                    
                            </div>
							 <div class="overlay_content text-center">
						   <?php if ($asset['famous-image-hover-title-four']) :?>
						    <h2><?php echo $asset['famous-image-hover-title-four'] ?> </h2>
							<?php else: ?>
								<h2>Mountainbike WM</h2>							
							<?php endif ?>
							 							 
							  <?php if ($asset['famous-image-hover-description-four']) :?>
							   <p><?php echo $asset['famous-image-hover-description-four'] ?> </p>
								<?php else: ?>	
						 <p>Branding Print WEB</p>								
								<?php endif ?>
							</div>
                        </div>
                    
                        <div class="element-item transition client smash " data-category="transition">
                       
                            <div class="project add-animation animation-2">
							<?php if ($asset['famous-image-five']['url']) : ?>
							<img src="<?php echo $asset['famous-image-five']['url'] ?> "/>							
							<?php else: ?>								
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/opt_pk_thumbnail.jpg"/>					
							<?php endif ?>
                                                                                          
                                
                            </div>
							 <div class="overlay_content text-center">
						   <?php if ($asset['famous-image-hover-title-five']) :?>
						    <h2><?php echo $asset['famous-image-hover-title-five'] ?> </h2>
							<?php else: ?>
								<h2>Mountainbike WM</h2>							
							<?php endif ?>
							 
							 
							  <?php if ($asset['famous-image-hover-description-five']) :?>
							   <p><?php echo $asset['famous-image-hover-description-five'] ?> </p>
								<?php else: ?>	
						 <p>Branding Print WEB</p>								
								<?php endif ?>
							</div>
                        </div>
                          
                        <div class="element-item alkali agent smash " data-category="alkali">
                       
                            <div class="project add-animation animation-3">
                             	<?php if ($asset['famous-image-six']['url']) : ?>
							<img src="<?php echo $asset['famous-image-six']['url'] ?> "/>							
							<?php else: ?>								
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/opt_alp_thumbnail.jpg"/>					
							<?php endif ?>
                                                               
                                
                            </div>
							 <div class="overlay_content text-center">
						   <?php if ($asset['famous-image-hover-title-six']) :?>
						    <h2><?php echo $asset['famous-image-hover-title-six'] ?> </h2>
							<?php else: ?>
								<h2>Mountainbike WM</h2>							
							<?php endif ?>
							 
							 
							  <?php if ($asset['famous-image-hover-description-six']) :?>
							   <p><?php echo $asset['famous-image-hover-description-six'] ?> </p>
								<?php else: ?>	
						 <p>Branding Print WEB</p>								
								<?php endif ?>
							</div>
                        </div>
                       
                       
                    </div>
                    
                    
                    
                    
                    
                   </div> 
                </div>
            </div>
			
			<!-- ffdsf -->
			
			
			 <div class="section section-numbers-1" id="numbers">
                <div class="container">
                    <div class="text-area">
                        <div class="title add-animation">
						<?php if ($asset['number-header']) :?>
						     <h2><?php echo $asset['number-header'] ?></h2>
							<?php else: ?>
							 <h2>Who’s ready</h2>							
							<?php endif ?>
                           
                            <div class="separator-container">
                                <div class="separator line-separator">∎</div>
                            </div>
							
							<?php if ($asset['number-description']) :?>
							 <p><?php echo $asset['number-description'] ?></p>
							<?php else: ?>	
							 <p>"Started from the bottom now we're here" - Drake </p>							
							<?php endif ?>
                           
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="card card-plain add-animation animation-1">
							    <?php if ($asset['number-item-one-title']) :?>
								 <h3><?php echo $asset['number-item-one-title'] ?></h3>
                                <?php else: ?>
								 <h3>Customers</h3>								
                                <?php endif ?>
                               
								<?php if ($asset['number-item-one-description']) :?>
								<h5><?php echo $asset['number-item-one-description'] ?></h5>
                                <?php else: ?>	
								<h5>Businesses and Professionals</h5>								
                                <?php endif ?>
                                
								
								 <?php if ($asset['number-item-one-number']) :?>
								 <div class="number">
                                  <?php echo $asset['number-item-one-number'] ?>
                                </div>
                                <?php else: ?>
                              <div class="number">
                                    43.500+
                                </div>								
                                <?php endif ?>
                                
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="card card-plain add-animation animation-2">
							<?php if ($asset['number-item-two-title']) :?>
							<h3><?php echo $asset['number-item-two-title']?></h3>
							<?php else: ?>
                              <h3>Total</h3>							
							<?php endif ?>
                                
								<?php if ($asset['number-item-two-description']) :?>
								<h5><?php echo $asset['number-item-two-description']?></h5>
								<?php else: ?>	
                                <h5>Freebies, Templates & UI Kits</h5>								
								<?php endif ?>
                                
								<?php if ($asset['number-item-two-number']) :?>
								<div class="number" >
                                   <?php echo $asset['number-item-two-number'] ?>
                                </div>
								<?php else: ?>
                                  <div class="number" >
                                    60.000+
                                </div>								
								<?php endif ?>
                                
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="card card-plain add-animation animation-3">
							<?php if ($asset['number-item-three-title']) :?>
							<h3><?php echo $asset['number-item-three-title'] ?></h3>
							<?php else: ?>		
							<h3>Agent</h3>
							<?php endif ?>
															
								<?php if ($asset['number-item-three-description']) :?>
								<h5><?php echo $asset['number-item-three-description'] ?></h5>
								<?php else: ?>
								<h5>Where we made sponsorships</h5>		
								<?php endif ?>
                                
								<?php if ($asset['number-item-three-number']) :?>
								<div class="number" >
                                  <?php echo $asset['number-item-three-number'] ?>
                                </div>
								<?php else: ?>		
								<div class="number" >
																	14
																</div>
								<?php endif ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
			
	    <div class="section section-header" id="uitools">
                <div class="parallax pattern-image">
				<?php if ($asset['signup-background']['url']) :?>
				
				<img src="<?php echo $asset['signup-background']['url'] ?>"/>
				
				<?php else: ?>	
				
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner.jpg"/>	
				
				<?php endif ?>
                    
               
                    <div class="container">				 
					 <div class="card content add-animation">
					 <?php if ($asset['signup-title']) :?>
					 <h2><?php echo $asset['signup-title']?></h2>
					<?php else: ?>	
					<h2>Sign me up</h2>	
					<?php endif ?>
					 
					 <?php if ($asset['signup-description']) :?>
					  <h5><?php echo $asset['signup-description'] ?></h5>
					<?php else: ?>	
					 <h5>Awesome Bootstrap templates to build better websites</h5>	
					<?php endif ?>
	               
                  <div class="form_cotent add-animation animation-4">
				  
					
				   <?php echo do_shortcode ('[contact-form-7 id="31" title="Untitled"] ') ?>
				
					
				     
                  </div>                                                                    
                        </div>                       
                    </div>
                </div>              
            </div>
			            
            <div class="section section-we-made-3" id="faq">
                <div class="container">
                    <div class="row">
                        <div class="title add-animation">
						<?php if ($asset['faq-title']) :?>
						 <h2><?php echo $asset['faq-title'] ?></h2>
						<?php else: ?>	
						<h2>FAQ</h2>	
						<?php endif ?>
                    
                            <div class="separator-container">
                                <div class="separator line-separator">∎</div>
                            </div>
							<?php if ($asset['faq-description']) :?>
							<p><?php echo $asset['faq-description'] ?><br><br></p>
							<?php else: ?>	
							<p>If have any Question first see our faq section<br><br></p>	
							<?php endif ?>
                            
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
					
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 card add-animation animation-4">
              <div class="panel-group  " id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <div class="panel-heading" role="tab" id="headingOne">
					<?php if ($asset['qus-1']) :?>
					<h4 class="panel-title">					  
                     <?php echo $asset['qus-1']?>
                     </h4>
					<?php else: ?>	
					<h4 class="panel-title">Making FAQ page accordion style? </h4>	
					<?php endif ?>                     
                    </div>
                  </a>
                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				  <?php if ($asset['ans-1']) :?>
				   <div class="panel-body">
                      <?php echo $asset['ans-1']?>
                    </div>
					<?php else: ?>	
					<div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>					
					<?php endif ?>

					
                  </div>
                </div>
                <div class="panel panel-default">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      
					  
					  <?php if ($asset['qus-2']) :?>
					<h4 class="panel-title">					  
                     <?php echo $asset['qus-2']?>
                     </h4>
					<?php else: ?>	
					<h4 class="panel-title"> Expand / Collapsing Blocks? </h4>	
					<?php endif ?>
					  
					  
                    </div>
                  </a>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <?php if ($asset['ans-2']) :?>
				   <div class="panel-body">
                      <?php echo $asset['ans-2']?>
                    </div>
					<?php else: ?>	
					<div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>					
					<?php endif ?>
                  </div>
                </div>
                <div class="panel panel-default">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <div class="panel-heading" role="tab" id="headingThree">
                      					  
					  <?php if ($asset['qus-3']) :?>
					<h4 class="panel-title">					  
                     <?php echo $asset['qus-3']?>
                     </h4>
					<?php else: ?>	
					<h4 class="panel-title"> How do I create hide/show FAQs in Squarespace (collapsed text) ?</h4>	
					<?php endif ?>
					  
                    </div>
                  </a>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <?php if ($asset['ans-3']) :?>
				   <div class="panel-body">
                      <?php echo $asset['ans-3']?>
                    </div>
					<?php else: ?>	
					<div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>					
					<?php endif ?>
                  </div>
                </div>
                <div class="panel panel-default">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                    <div class="panel-heading" role="tab" id="headingThree">
					  
					<?php if ($asset['qus-4']) :?>
					<h4 class="panel-title">					  
                     <?php echo $asset['qus-4']?>
                     </h4>
					<?php else: ?>	
					<h4 class="panel-title">HTML loads before JQuery, help disabling that code?</h4>	
					<?php endif ?>
					  
                    </div>
                  </a>
                  <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <?php if ($asset['ans-4']) :?>
				   <div class="panel-body">
                      <?php echo $asset['ans-4']?>
                    </div>
					<?php else: ?>	
					<div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>					
					<?php endif ?>
                  </div>
                </div>
                <div class="panel panel-default">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                    <div class="panel-heading" role="tab" id="headingThree">
                      
					<?php if ($asset['qus-5']) :?>
					<h4 class="panel-title">					  
                     <?php echo $asset['qus-5']?>
                     </h4>
					<?php else: ?>	
					<h4 class="panel-title">Need help adding an accordion.</h4>	
					<?php endif ?>
                    </div>
                  </a>
                  <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                     <?php if ($asset['ans-5']) :?>
				   <div class="panel-body">
                      <?php echo $asset['ans-5']?>
                    </div>
					<?php else: ?>	
					<div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>					
					<?php endif ?>
                  </div>
                </div>
                <div class="panel panel-default">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                    <div class="panel-heading" role="tab" id="headingThree">
                    
					  <?php if ($asset['qus-6']) :?>
					<h4 class="panel-title">					  
                     <?php echo $asset['qus-6']?>
                     </h4>
					<?php else: ?>	
					<h4 class="panel-title">Can I create an accordion element? </h4>	
					<?php endif ?>
                    </div>
                  </a>
                  <div id="collapsesix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <?php if ($asset['ans-6']) :?>
				   <div class="panel-body">
                      <?php echo $asset['ans-6']?>
                    </div>
					<?php else: ?>	
					<div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>					
					<?php endif ?>
                  </div>
                </div>
              </div>
            </div>					    
                    </div>
    
                </div>
            </div>
            
			
            <div class="section section-contact-3" id="contact">
                <div class="contact-container">
                    <div class="address-container add-animation animation-1">
                        <div class="address">
						<?php if ($asset['contact-title']) :?>
						<h4><?php echo $asset['contact-title'] ?></h4>
						<?php else: ?>
						<h4>Where to meet ?</h4>		
						<?php endif ?>
                            
							<?php if ($asset['contact-address']) :?>
							<p class="text-gray"><?php echo $asset['contact-address'] ?>
                            </p>
							<?php else: ?>
							<p class="text-gray">
						Bld. Mihail Kogalniceanu nr. 8<br>
						Bucharest, Romania</p>		
							<?php endif ?>
                            
							
							<?php if ($asset['say-hellow']) :?>
							 <h4><?php echo $asset['say-hellow'] ?></h4>
							<?php else: ?>	
							 <h4>Want to say hello?</h4>	
							<?php endif ?>
													   
							<?php if ($asset['contact-page']) :?>
							<a target="_blank" href="<?php echo $asset['contact-page'] ?>"  class="btn btn-black btn-contact">
                                CONTACT US <i class="fa fa-paper-plane"></i>
                            </a>
							<?php else: ?>
							<a target="_blank" href="http://assetprotectionenterprise.com/contact-us"  class="btn btn-black btn-contact">
								CONTACT US <i class="fa fa-paper-plane"></i></a>		
							<?php endif ?>
                            
							
							<?php if ($asset['social-sharing']) :?>
							 <h4><?php echo $asset['social-sharing'] ?></h4>
							<?php else: ?>
							 <h4>Social sharing!</h4>		
							<?php endif ?>
 							
						<div class="social-area">   

                           <?php if ($asset['facebook']) :?>
							 <a class="btn btn-social btn-facebook btn-simple" href="<?php echo $asset['facebook'] ?>" target="_blank">
                               <i class="fa fa-facebook-square"></i>
                            </a>
							<?php else: ?>															
                            <a class="btn btn-social btn-facebook btn-simple" href="https://www.facebook.com/" target="_blank">
                               <i class="fa fa-facebook-square"></i>
                            </a>							
							<?php endif ?>
							
							<?php if ($asset['twitter']) :?>
							<a class="btn btn-social btn-twitter btn-simple" href="<?php echo $asset['twitter'] ?>" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
							<?php else: ?>	
							<a class="btn btn-social btn-twitter btn-simple" href="https://twitter.com/" target="_blank">
							<i class="fa fa-twitter"></i></a>	
							<?php endif ?>
                            
							<?php if ($asset['linkedin']) :?>
							  <a class="btn btn-social btn-pinterest btn-simple" href="<?php echo $asset['linkedin']?>" target="_blank">
                                 <i class="fa fa-linkedin-square"></i>
                            </a>
							<?php else: ?>
							  <a class="btn btn-social btn-pinterest btn-simple" href="https://www.linkedin.com" target="_blank">
							<i class="fa fa-linkedin-square"></i></a>		
							<?php endif ?>
								
							<?php if ($asset['pinterest']) :?>
							<a class="btn btn-social btn-pinterest btn-simple" href="<?php echo $asset['pinterest'] ?>" target="_blank">
							<i class="fa fa-pinterest-square"></i></a>
							<?php else: ?>	
							<a  class="btn btn-social btn-pinterest btn-simple" href="https://www.pinterest.com/" target="_blank">
							<i class="fa fa-pinterest-square"></i></a>	
							<?php endif ?>

                    </div>
														
                        </div>
                    </div>
                    <div  class="map">
                        <div id="contactUsMap" class="big-map"></div>
                    </div>
                </div>
            </div>  
			 <footer class="footer footer-color-black">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#sliders">
                                Home
                                </a>
                            </li>
                            <li>
							<?php if ($asset['contact-page']) :?>
                                <a target="_blank" href="<?php echo $asset['contact-page'] ?>">
                                Contact
                                </a>
								<?php else: ?>	
								<a target="_blank" href="#">
                                Contact
                                </a>
								<?php endif ?>
                            </li>
                                                      
                        </ul>
                    </nav>
                    <div class="social-area pull-right">                
                      
                           <?php if ($asset['facebook']) :?>
							 <a class="btn btn-social btn-facebook btn-simple" href="<?php echo $asset['facebook'] ?>" target="_blank">
                               <i class="fa fa-facebook-square"></i>
                            </a>
							<?php else: ?>															
                            <a class="btn btn-social btn-facebook btn-simple" href="https://www.facebook.com/" target="_blank">
                               <i class="fa fa-facebook-square"></i>
                            </a>							
							<?php endif ?>
																					
							<?php if ($asset['twitter']) :?>
							<a class="btn btn-social btn-twitter btn-simple" href="<?php echo $asset['twitter'] ?>" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
							<?php else: ?>	
							<a class="btn btn-social btn-twitter btn-simple" href="https://twitter.com/" target="_blank">
							<i class="fa fa-twitter"></i></a>	
							<?php endif ?>
                            
                            
							<?php if ($asset['linkedin']) :?>
							  <a class="btn btn-social btn-pinterest btn-simple" href="<?php echo $asset['linkedin']?>" target="_blank">
                                 <i class="fa fa-linkedin-square"></i>
                            </a>
							<?php else: ?>
							  <a class="btn btn-social btn-pinterest btn-simple" href="https://www.linkedin.com" target="_blank">
							<i class="fa fa-linkedin-square"></i></a>		
							<?php endif ?>
								
							<?php if ($asset['pinterest']) :?>
							<a class="btn btn-social btn-pinterest btn-simple" href="<?php echo $asset['pinterest'] ?>" target="_blank">
							<i class="fa fa-pinterest-square"></i></a>
							<?php else: ?>	
							<a  class="btn btn-social btn-pinterest btn-simple" href="https://www.pinterest.com/" target="_blank">
							<i class="fa fa-pinterest-square"></i></a>	
							<?php endif ?>
                    </div>
					<?php if ($asset['footer-text']) :?>
					 <div class="copyright"><?php echo $asset['footer-text'] ?>
                    </div>
					<?php else: ?>
					 <div class="copyright">
											&copy; 2015 <a target="_blank" href="http://webdesignernet.com">ASSET PROTECTION ENTERPRISE</a>, page made with <a target="_blank" href="http://webdesignernet.com">ASSET PROTECTION ENTERPRISE</a>
										</div>		
					<?php endif ?>					                  
                </div>
            </footer>
            
        </div> <!-- end wrapper -->

    <!--   core js files    -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.js" type="text/javascript"></script>
    <!--   file for adding vertical points where we activate the elements animation   -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.waypoints.min.js"></script>
    <!--  js library for devices recognition -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.js"></script>
    <!--  script for google maps   -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <!--   file where we handle all the script from the Rubik page   -->
   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/isotope.min.js"></script>
   
   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/rubick_pres.js"></script>
   
 <?php wp_footer();?>
    </body>
</html>
