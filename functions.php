<?php

// jequery call
function asset_jequery() {
    wp_enqueue_script('jquery');
}
add_action('init', 'asset_jequery');


function asset_functions(){
	
	add_theme_support('title-tag');
}
add_action('after_setup_theme','asset_functions');


if(file_exists(dirname(__FILE__).'/lib/ReduxCore/framework.php')){
	
	require_once(dirname(__FILE__).'/lib/ReduxCore/framework.php');
	
}

if (file_exists(dirname(__FILE__).'/lib/sample/assetonepage.php')){
	
	require_once(dirname(__FILE__).'/lib/sample/assetonepage.php');
	
}





